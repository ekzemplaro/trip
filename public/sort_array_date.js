// -----------------------------------------------------------------------
//	sort_array_date.js
//
//						May/29/2023
//
// -----------------------------------------------------------------------
'use strict'

// -----------------------------------------------------------------------
function sort_array_date_proc (array_aa)
{
	array_aa.sort(compare_by_date_proc)

	return array_aa
}

// ---------------------------------------------------------------
function compare_by_date_proc(left,right)
{
	const aa = new Date(left.date)
	const bb = new Date(right.date)

	var rvalue = 0

	if (aa > bb)
		{
		rvalue = -1
		}
	else if (aa < bb)
		{
		rvalue = 1
		}

	return	rvalue
}

// -----------------------------------------------------------------------
