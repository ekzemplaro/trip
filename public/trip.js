// -----------------------------------------------------------------------
//	trip.js
//
//					May/29/2023
//
// -----------------------------------------------------------------------
'use strict'
// -----------------------------------------------------------------------
window.onload = ()=>
{
	document.querySelector("#outarea_aa").innerText = "*** trip.js *** start ***"

	const file_json = "./data.json"
	const place = ".contents"
	read_fetch_table_proc(file_json,place)

	document.querySelector("#outarea_hh").innerText = "*** trip.js *** end ***"

}

// -----------------------------------------------------------------------
function read_fetch_table_proc(url,place)
{
	fetch(url).then((response) => {
		if(!response.ok) {
			console.log('Read error!')
			throw new Error('error')
		} 
		console.log('Read ok!')
		return response.text()
	}).then((data)  => {
//		console.log(data)
		const data_aa = JSON.parse(data)

		const array_aa = sort_array_date_proc(data_aa)

		show_table_proc(place,array_aa)
})

}
// -----------------------------------------------------------------------
// [4]:
function show_table_proc(place,data_aa)
{
	var str_out = ""
	str_out += "<table>"
	str_out += "<tr>"
	str_out += "<th>No</th>"
	str_out += "<th>日付</th>"
	str_out += "<th>起点</th>"
	str_out += "<th>終点</th>"
	str_out += "</tr>"

	var no = 0
	data_aa.forEach(function (aax)
		{
		if (aax.itenery[0].from != "")
			{
			no += 1
			}
		})

	data_aa.forEach(function (aax)
		{
		if (aax.itenery[0].from != "")
			{
			str_out += record_proc(aax,no)
			no -= 1
			}
		})

	str_out += "</table>"

	document.querySelector(place).innerHTML = str_out
}

// -----------------------------------------------------------------------
function record_proc(aax,no)
{
	var str_out = "<tr>"
	const llx = (aax.itenery).length
	str_out += "<td rowspan=" + llx + ">" + no + "</td>"
	str_out += "<td rowspan=" + llx + ">" + aax.date + "</td>"

	for (var it in aax.itenery)
		{
		str_out += "<td>" + aax.itenery[it].from + "</td>"
		str_out += "<td>" + aax.itenery[it].to + "</td>"
		str_out += "</tr>"
		}
	str_out += "</tr>"

	return str_out
}

// -----------------------------------------------------------------------
